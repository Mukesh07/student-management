package com.example.mukesh.studentmanagement.utils;

import com.example.mukesh.studentmanagement.Student.Student;

/**
 * Created by Mukesh on 9/29/2015.
 */
public interface EndFragment {
    public void endFragment(int choice,Student student);

}
