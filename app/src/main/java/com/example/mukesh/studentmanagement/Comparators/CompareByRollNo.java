package com.example.mukesh.studentmanagement.Comparators;

import com.example.mukesh.studentmanagement.Student.Student;

import java.util.Comparator;

/**
 * Created by Mukesh on 9/10/2015.
 */
public class CompareByRollNo implements Comparator<Student> {

    @Override
    public int compare(Student lhs, Student rhs) {
        int rollNo1= lhs.getRoll_no();
        int rollNo2= rhs.getRoll_no();
        if (rollNo1>rollNo2)
            return 1;
        else
        return -1;
    }
}
