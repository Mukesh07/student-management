package com.example.mukesh.studentmanagement;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.mukesh.studentmanagement.Student.Student;

import java.util.LinkedList;

/**
 * Created by Mukesh on 9/16/2015.
 */
public class DBController implements DBConstants {
    DBHelper helper;
    SQLiteDatabase db;

    public DBController(Context context) {
        helper = new DBHelper(context, DATABASE_NAME, null, 6);

    }

    public long insertDetails(Student student) {
         db = helper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DETAILS_COLUMN_NAME, student.getName());
        contentValues.put(DETAILS_COLUMN_PHONE, student.getContact());
        contentValues.put(DETAILS_COLUMN_ADDRESS, student.getAddress());
       // contentValues.put(DETAILS_COLOUMN_ID, student.getRoll_no());
        long i = db.insert(DETAILS_TABLE_NAME, null, contentValues);
        db.close();
        return i;
    }
 public LinkedList<Student> viewAll( LinkedList<Student>student1)
    {
        student1.clear();
        String query = "SELECT * FROM "+DETAILS_TABLE_NAME+ ";";
        db = helper.getWritableDatabase();
        Cursor cursor = db.rawQuery(query,null);
        Student stud = null;
       if(cursor.moveToFirst())
       {
           do
           {
               stud = new Student();
               stud.setRoll_no(Integer.parseInt(cursor.getString(1)));
               stud.setName(cursor.getString(0));
               stud.setContact(Long.parseLong(cursor.getString(2)));
               stud.setAddress(cursor.getString(3));
               student1.add(stud);
           }while(cursor.moveToNext());
       }
        db.close();
        return student1;
    }
    public int updateDetails(Student student)
    {
        db = helper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DETAILS_COLUMN_NAME, student.getName());
        contentValues.put(DETAILS_COLUMN_PHONE, student.getContact());
        contentValues.put(DETAILS_COLUMN_ADDRESS, student.getAddress());
        int value = db.update(DBHelper.DETAILS_TABLE_NAME, contentValues, DBHelper.DETAILS_COLOUMN_ID + " =? " , new String[]{String.valueOf(student.getRoll_no())});
        db.close();
        return value;
    }
    public void deleteDetails(Student student)
    {
        db = helper.getWritableDatabase();
        db.delete(DBHelper.DETAILS_TABLE_NAME, DBHelper.DETAILS_COLOUMN_ID + " =?", new String[]{String.valueOf(student.getRoll_no())});
        db.close();
    }
}
