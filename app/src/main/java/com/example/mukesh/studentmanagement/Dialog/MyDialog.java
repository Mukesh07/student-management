package com.example.mukesh.studentmanagement.Dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.example.mukesh.studentmanagement.R;
import com.example.mukesh.studentmanagement.utils.DialogListener;


/**
 * Created by Mukesh on 9/8/2015.
 */
public class MyDialog extends DialogFragment implements View.OnClickListener {
    LayoutInflater inflater;
    View v;
   DialogListener dialogListener;
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        inflater =getActivity().getLayoutInflater();
        v=inflater.inflate(R.layout.dialog, null);
        AlertDialog.Builder builder= new AlertDialog.Builder(getActivity());
        builder.setView(v);
        final Button button4 = (Button) v.findViewById(R.id.button4);
        final Button button5 = (Button) v.findViewById(R.id.button5);
        final Button button6 = (Button) v.findViewById(R.id.button6);
        button4.setOnClickListener(this);
        button5.setOnClickListener(this);
        button6.setOnClickListener(this);
        return builder.create();
 }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.button4:
                dialogListener.OnClick(1);
                dismiss();

                break;
            case R.id.button5:
                dialogListener.OnClick(2);
                dismiss();
                break;
            case R.id.button6:
                dialogListener.OnClick(3);
                dismiss();
                break;
        }

    }
    public void setDialogListener(DialogListener dialogListener) {
        this.dialogListener = dialogListener;

    }
}

