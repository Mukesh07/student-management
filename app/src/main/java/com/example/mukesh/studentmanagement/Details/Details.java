package com.example.mukesh.studentmanagement.Details;

import android.content.Context;
import android.content.Intent;
import android.provider.SyncStateContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

//import com.example.mukesh.studentmanagement.DBHelper;
import com.example.mukesh.studentmanagement.R;
import com.example.mukesh.studentmanagement.Student.Student;
import com.example.mukesh.studentmanagement.utils.EndFragment;
import com.example.mukesh.studentmanagement.utils.StartFragment;
import com.example.mukesh.studentmanagement.utils.constants;

public class Details extends Fragment implements View.OnClickListener, StartFragment, constants {


    EditText editText = null;
    //    EditText editText2 = null;
    EditText editText3 = null;
    EditText editText4 = null;
    //    EditText editText6 = null;
    Student student;
    EndFragment endFragObj;
    //private DBHelper dbHelper;
    Boolean isEditable = false;
    View myView;
    Student st1;
    Button button2, button3;
    InputMethodManager imm;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        editText = (EditText) myView.findViewById(R.id.editText);
        editText3 = (EditText) myView.findViewById(R.id.editText3);
        editText4 = (EditText) myView.findViewById(R.id.editText4);
        TextView textView5 = (TextView) myView.findViewById(R.id.textView5);
        button2 = (Button) myView.findViewById(R.id.button2);
        button3 = (Button) myView.findViewById(R.id.button3);
        endFragObj = (EndFragment) getActivity();
        button2.setOnClickListener(this);
        button3.setOnClickListener(this);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup container, Bundle savedInstance) {
        myView = layoutInflater.inflate(R.layout.activity_details, container, false);
        return myView;
    }

    @Override
    public void onClick(View v) {
        String name = editText.getText().toString();
        String contact = editText3.getText().toString();
        String address = editText4.getText().toString();

        switch (v.getId()) {
            case R.id.button3:
                if ((!name.isEmpty()) && (!contact.isEmpty()) && (!address.isEmpty())) {
                    if (isEditable) {
//                        st1.setRoll_no();
                        st1.setName(name);
                        st1.setContact(Long.parseLong(contact));
                        st1.setAddress(address);
                        endFragObj.endFragment(UPDATE_ASYNC, st1);
                    } else {
                        st1 = new Student();
                        st1.setRoll_no(0);
                        st1.setName(name);
                        st1.setContact(Long.parseLong(contact));
                        st1.setAddress(address);
                        endFragObj.endFragment(ADD_ASYNC, st1);
                    }
                }
                break;
            case R.id.button2:
                clearEdittext();
                closeKeyboard(getView());
                endFragObj.endFragment(0, null);
                break;
        }
    }

    @Override
    public void btChoice(int toDo, Student stu) {
        st1=stu;
        if (stu != null) {
            editText.setText(stu.getName());
            editText3.setText(String.valueOf(stu.getContact()));
            editText4.setText(stu.getAddress());

        }
        switch (toDo) {
            case VIEW_SPECIFIC_ASYNC:
                isEditable = false;
                editText.setEnabled(isEditable);
                editText3.setEnabled(isEditable);
                editText4.setEnabled(isEditable);
                button3.setVisibility(View.INVISIBLE);
                break;

            case VIEW_UPDATE_ASYNC:
                isEditable = true;
                editText.setEnabled(isEditable);
                editText3.setEnabled(isEditable);
                editText4.setEnabled(isEditable);
                editText.setSelection(editText.getText().length());
                button3.setVisibility(View.VISIBLE);
                openKeyboard();
                break;
        }
    }

    void clearEdittext() {
        editText.setText("");
        editText3.setText("");
        editText4.setText("");
    }


    void openKeyboard() {
        editText.requestFocus();
        imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, InputMethodManager.SHOW_FORCED);
    }


    void closeKeyboard(View v) {
        imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }
}