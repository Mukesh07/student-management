package com.example.mukesh.studentmanagement.Adapters;

import android.content.Context;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.LinkedList;

import android.view.ViewGroup;

import com.example.mukesh.studentmanagement.R;
import com.example.mukesh.studentmanagement.Student.Student;

/**
 * Created by Mukesh on 9/8/2015.
 */
public class MyAdapter extends BaseAdapter  {
    LinkedList<Student> data;
    Context context;
    int rowLayout;

    public MyAdapter(LinkedList<Student> data, Context context, int rowLayout) {
        this.data = data;
        this.context = context;
        this.rowLayout = rowLayout;
}
    @Override
    public int getCount() {
        return data == null?0:data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = View.inflate(context, rowLayout, null);
        TextView textName = (TextView) convertView.findViewById(R.id.t1);
        TextView textRoll = (TextView) convertView.findViewById(R.id.t2);
        TextView textContact = (TextView) convertView.findViewById(R.id.t3);
        TextView textAddress = (TextView) convertView.findViewById(R.id.t4);
        textName.setText(data.get(position).getName());
        textRoll.setText(String.valueOf(data.get(position).getRoll_no()));
        textContact.setText(String.valueOf(data.get(position).getContact()));
        textAddress.setText(data.get(position).getAddress());
        return convertView;
    }
}
