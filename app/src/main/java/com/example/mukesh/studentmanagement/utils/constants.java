package com.example.mukesh.studentmanagement.utils;

/**
 * Created by Mukesh on 9/16/2015.
 */
public interface constants {
    int add=1,addview=10,addedit=20;
    int view=1, edit=2, delete=3;
    int ADD_STUDENT = 11;
    int DIALOG_EDIT = 12;
    int DIALOG_VIEW = 13;
    int UPDATE = 8;
    int VIEW_SPECIFIC_CODE = 5;
    int VIEW_ALL_CODE = 9;
    int DELETE_STUDENT = 14;
    int ADD_ASYNC=1,UPDATE_ASYNC=2;
    int  VIEW_SPECIFIC_ASYNC=3,VIEW_UPDATE_ASYNC=6;
    int  VIEW_ALL_ASYNC=4;
    int  DELETE_ASYNC=5;
    String DATA_DELETED = "Data deleted";
}
