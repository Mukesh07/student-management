package com.example.mukesh.studentmanagement.Comparators;

import com.example.mukesh.studentmanagement.Student.Student;

import java.util.Comparator;

/**
 * Created by Mukesh on 9/10/2015.
 */
public class CompareByName implements Comparator<Student> {
    @Override
    public int compare(Student lhs, Student rhs) {
        String student1=lhs.getName();
        String student2=rhs.getName();
        int result = student1.compareToIgnoreCase(student2);
        if(result>0)
        {
            return 1;
        }
        else
        {
            return -1;

    }
}
}
