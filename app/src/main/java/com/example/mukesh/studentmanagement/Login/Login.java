package com.example.mukesh.studentmanagement.Login;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.mukesh.studentmanagement.Main.MainActivity;
import com.example.mukesh.studentmanagement.R;


public class Login extends AppCompatActivity implements View.OnClickListener {
    EditText username, password;
    public static final String namekey = "Mukesh";
    public static final String passkey = "12345";
    Button login1, exit;
    SharedPreferences preferences;
    //Intent intent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        username = (EditText) findViewById(R.id.name);
        password = (EditText) findViewById(R.id.pass);
        login1 = (Button) findViewById(R.id.login);
        exit = (Button) findViewById(R.id.exit);
        login1.setOnClickListener(this);
        exit.setOnClickListener(this);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        String FlagValue = preferences.getString("Flag Value", "");

        if (FlagValue.equals("True")) {
            Intent intent3 = new Intent(this, MainActivity.class);
            startActivity(intent3);
            finish();
        }
        String getname = preferences.getString("key1", "");
        String getpass = preferences.getString("key2", "");
        username.setText(getname);
        password.setText(getpass);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login:
                String name = username.getText().toString();
                String pass = password.getText().toString();
                if (name.equals(namekey) && pass.equals(passkey)) {
                    SharedPreferences.Editor editit = preferences.edit();
                    editit.putString("key1", name);
                    editit.putString("key2", pass);
                    editit.putString("Flag Value", "True");
                    editit.commit();
                    Intent intent3 = new Intent(this, MainActivity.class);
                    startActivity(intent3);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Enter Valid Details", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.exit:
                finish();
                break;

        }
    }

}


