package com.example.mukesh.studentmanagement.Main;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.example.mukesh.studentmanagement.Adapters.MyAdapter;
import com.example.mukesh.studentmanagement.Comparators.CompareByName;
import com.example.mukesh.studentmanagement.Comparators.CompareByRollNo;
import com.example.mukesh.studentmanagement.DBController;
import com.example.mukesh.studentmanagement.Details.Details;
import com.example.mukesh.studentmanagement.Dialog.MyDialog;
import com.example.mukesh.studentmanagement.Login.Login;
import com.example.mukesh.studentmanagement.R;
import com.example.mukesh.studentmanagement.Student.Student;
import com.example.mukesh.studentmanagement.utils.DialogListener;
import com.example.mukesh.studentmanagement.utils.EndFragment;
import com.example.mukesh.studentmanagement.utils.constants;


import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements DialogListener, EndFragment, constants {
    RelativeLayout parent, fr1;
    MyAdapter adapter;
    static int studentId = 1;
    Intent intent;
    LinkedList<Student> listData;
    int positionClicked;
    SharedPreferences preferences;
    Button logout, button, list;
    Student object;
    //DBController controller;
    FragmentTransaction fragmentTransaction;
    ListView listView;
    FragmentManager fragmentManager;
    Details detailsFragment;
    DrawerLayout dLayout;
    ListView dList;
    YAdapter Yadapt;
    String[] menu;
    ActionBarDrawerToggle drawerListener;

    // Fragment detailsFragment;
    //DBHelper dbHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        menu = getResources().getStringArray(R.array.menu);
        dList = (ListView) findViewById(R.id.left_drawer);
        Yadapt = new YAdapter(this);
        dList.setAdapter(Yadapt);
        dLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerListener = new ActionBarDrawerToggle(this, dLayout, R.string.opendrawer, R.string.closedrawer) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        dLayout.setDrawerListener(drawerListener);

        parent = (RelativeLayout) findViewById(R.id.parent);
        fr1 = (RelativeLayout) findViewById(R.id.fr1);
        //controller = new DBController(this);
        listData = new LinkedList<>();
        //listData = controller.viewAll(listData);
        //  dbHelper = new DBHelper(this);
        listView = (ListView) findViewById(R.id.listView);
        final GridView gridView = (GridView) findViewById(R.id.gridView);

        list = (Button) findViewById(R.id.list);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        adapter = new MyAdapter(listData, this, R.layout.list);
        listView.setAdapter(adapter);
        new Async().execute(VIEW_ALL_ASYNC);



        list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gridView.setVisibility(v.INVISIBLE);
                listView.setVisibility(v.VISIBLE);
                listView.setAdapter(adapter);
            }
        });
        final Button grid = (Button) findViewById(R.id.grid);
        grid.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        listView.setVisibility(v.INVISIBLE);
                                        gridView.setVisibility(v.VISIBLE);
                                        gridView.setAdapter(adapter);
                                        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                            @Override
                                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                                positionClicked = position;
                                                MyDialog dialog = new MyDialog();
                                                dialog.setDialogListener(MainActivity.this);
                                                dialog.show(getFragmentManager(), "my_dialog");

                                            }
                                        });
                                    }
                                }
        );


        final Spinner spinner = (Spinner) findViewById(R.id.spinner);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String sortBy = spinner.getSelectedItem().toString();

                if (sortBy.equals("Name")) {
                    Collections.sort(listData, new CompareByName());
                    adapter.notifyDataSetChanged();

                } else if (sortBy.equals("Roll_No")) {
                    Collections.sort(listData, new CompareByRollNo());
                    adapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        dList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dLayout.closeDrawers();
                switch (position) {
                    case 0:
                        callFragment();
                        lockNavigationBar();
                        break;

                    case 1:
                        preferences.edit().clear().commit();
                        intent = new Intent(MainActivity.this, Login.class);
                        startActivity(intent);
                        finish();
                        break;
                }
                selectItem(position);
            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                positionClicked = position;
                MyDialog dialog = new MyDialog();
                dialog.setDialogListener(MainActivity.this);
                dialog.show(getFragmentManager(), "my_dialog");
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerListener.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerListener.onConfigurationChanged(newConfig);
    }

    public void selectItem(int position) {
        dList.setItemChecked(position, true);
    }

    public void setTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerListener.syncState();
    }

    void lockNavigationBar() {
        dLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        dLayout.closeDrawer(dList);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);
    }

    void unlockNavigationBar() {
        dLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    public void callFragment() {
        detailsFragment = new Details();
        fr1.setVisibility(View.GONE);
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.parent, detailsFragment, "DETAILS");
        fragmentTransaction.commit();

    }


    @Override
    public void OnClick(int position) {
        switch (position) {
            case 1:
                callFragment();
                lockNavigationBar();
                new Async().execute(VIEW_SPECIFIC_ASYNC);
                break;
            case 2:
                callFragment();
                lockNavigationBar();
                new Async().execute(VIEW_UPDATE_ASYNC);
                break;
            case 3:
                new Async().execute(DELETE_ASYNC);
                adapter.notifyDataSetChanged();
                Toast.makeText(this, DATA_DELETED, Toast.LENGTH_SHORT).show();
                break;

        }
    }


    @Override
    public void endFragment(int choice, Student student) {
        unlockNavigationBar();

        fragmentManager.beginTransaction().remove(detailsFragment).commit();
        fr1.setVisibility(View.VISIBLE);
        object = student;

        switch (choice) {
            case ADD_ASYNC:
                listData.add(object);
                new Async().execute(ADD_ASYNC);
                break;
            case UPDATE_ASYNC:
                new Async().execute(UPDATE_ASYNC);
                break;
            case 0:
                break;
        }

    }

    public class Async extends AsyncTask<Integer, Integer, Integer> {
        DBController controller;
        MyAdapter adap;
        int choice;
        Student tempStu;
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            tempStu = new Student();
            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setTitle("Loading...");
            progressDialog.show();
            controller = new DBController(MainActivity.this);
            adap = (MyAdapter) listView.getAdapter();

        }


        @Override
        protected Integer doInBackground(Integer... params) {
            choice = params[0];
            switch (choice) {
                case VIEW_ALL_ASYNC:
                    controller.viewAll(listData);
                    break;
                case ADD_ASYNC:
                    controller.insertDetails(listData.getLast());
                    break;
                case UPDATE_ASYNC:
                    controller.updateDetails(object);
                    break;
                case VIEW_SPECIFIC_ASYNC:
                    tempStu = listData.get(positionClicked);
                    break;
                case VIEW_UPDATE_ASYNC:
                    tempStu = listData.get(positionClicked);
                    break;
                case DELETE_ASYNC:
                    Student idStu = listData.get(positionClicked);
                    controller.deleteDetails(idStu);
                    listData.remove(positionClicked);
                    break;
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);

        }

        @Override
        protected void onPostExecute(Integer result) {
            Details f1;
            switch (choice) {
                case ADD_ASYNC:
                    new Async().execute(VIEW_ALL_ASYNC);
                    break;
                case VIEW_SPECIFIC_ASYNC:
                                f1 = (Details) fragmentManager.findFragmentByTag("DETAILS");
                    f1.btChoice(VIEW_SPECIFIC_ASYNC, tempStu);

                    break;
                case VIEW_UPDATE_ASYNC:
                    f1 = (Details) fragmentManager.findFragmentByTag("DETAILS");
                    f1.btChoice(VIEW_UPDATE_ASYNC, tempStu);

                    break;
            }
            progressDialog.dismiss();
            adap.notifyDataSetChanged();

//            adap.notifyDataSetChanged();
        }
    }

}

class YAdapter extends BaseAdapter {
    private Context context;
    String[] menuOne;
    int[] images = {R.mipmap.user_add,
            R.mipmap.exit,};

    public YAdapter(Context context) {
        this.context = context;
        menuOne = context.getResources().getStringArray(R.array.menu);
    }

    @Override
    public int getCount() {
        return menuOne.length;
    }

    @Override
    public Object getItem(int position) {
        return menuOne[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.custom_row, parent, false);
        } else {
            row = convertView;
        }
        TextView textView1 = (TextView) row.findViewById(R.id.textView);
        ImageView imageView1 = (ImageView) row.findViewById(R.id.imageView);
        textView1.setText(menuOne[position]);
        imageView1.setImageResource(images[position]);

        return row;
    }

}



