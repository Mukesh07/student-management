package com.example.mukesh.studentmanagement.Student;

import java.io.Serializable;

/**
 * Created by Mukesh on 9/8/2015.
 */
public class Student implements Serializable {
   public String name,address;
   public int roll_no;
     public       long contact;


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getRoll_no() {
        return roll_no;
    }

    public void setRoll_no(int roll_no) {
        this.roll_no = roll_no;
    }

    public long getContact() {
        return contact;
    }

    public void setContact(long contact) {
        this.contact = contact;
    }



    public String getName() {
        return name;

    }

    public void setName(String name) {
        this.name = name;
    }
}
