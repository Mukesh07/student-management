package com.example.mukesh.studentmanagement;

/**
 * Created by Mukesh on 9/16/2015.
 */
public interface DBConstants {
    String DATABASE_NAME = "MyDatabase.db";
    String DETAILS_TABLE_NAME = "details";
    String DETAILS_COLOUMN_ID = "rollno";
    String DETAILS_COLUMN_NAME = "name";
    String DETAILS_COLUMN_PHONE = "phone";
    String DETAILS_COLUMN_ADDRESS = "address";
    String CREATE_TABLE = "CREATE TABLE " + DETAILS_TABLE_NAME + "( " + DETAILS_COLUMN_NAME + " VARCHAR(255), "
            + DETAILS_COLOUMN_ID + " INTEGER PRIMARY KEY , " + DETAILS_COLUMN_PHONE + " NUMBER , "
            + DETAILS_COLUMN_ADDRESS + " VARCHAR(255) );";
    String DROP_TABLE = "DROP TABLE IF EXISTS " + DETAILS_TABLE_NAME + ";";
    int DATABASE_VERSION = 6;
}
